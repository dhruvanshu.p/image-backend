

import LtoL from '../components/convert_file/localtolocal.vue'
import LtoG from '../components/convert_file/localtogdrive.vue'
import GtoL from '../components/convert_file/gdrivetolocal.vue'
import GtoG from '../components/convert_file/gdrivetogdrive.vue'

import { createRouter, createWebHashHistory } from 'vue-router'

const routes = [
    {path: '/', component:LtoL, name:"LTOL" },
    {path: '/ltog', component:LtoG, name:"LTOG" },
    {path: '/gtog', component:GtoG, name:"GTOG" },
    {path: '/gtol', component:GtoL, name:"GTOL" },

]


const router = createRouter({
    history: createWebHashHistory(),
    routes, 
})

export default router