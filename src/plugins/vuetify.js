import Vue from 'vue'
import Vuetify from 'vuetify'
import { createVuetify } from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.config.productionTip = false

Vue.use(Vuetify)

const opts = {}

export default new createVuetify(opts)