import { createApp } from 'vue'
import App from './App.vue'
import { loadFonts } from './plugins/webfontloader'
import vue3GoogleLogin from 'vue3-google-login'



import router from './routers/index'
loadFonts()

const gAuthOptions = { clientId: '728619063252-gtj2nl5lj1n0khr8dq2hjp18bdnfblm7.apps.googleusercontent.com', scope:"https://www.googleapis.com/auth/drive", access_type:"offline" }



createApp(App)
  .use(router).use(vue3GoogleLogin, gAuthOptions)
  .mount('#app')
